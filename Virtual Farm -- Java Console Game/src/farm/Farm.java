package farm;

import animals.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class Farm implements Market {
    static Scanner scanner = new Scanner(System.in);
    private double money = 0;
    private boolean isSheep = false;
    private boolean isCow = false;
    private boolean isChicken = false;
    private boolean isHorse = false;
    public boolean isAllAnimals = false;
    private boolean isFeed = false;
    public int animalInList;
    private Cow cow;
    private Chicken chicken;
    private Sheep sheep;
    private Horse horse;
    private List<Sheep> sheepList = new ArrayList<>();
    private List<Cow> cowList = new ArrayList<>();
    private List<Chicken> chickenList = new ArrayList<>();
    private List<Horse> horseList = new ArrayList<>();

    public List<Sheep> getSheepList() {
        return sheepList;
    }
    public void setNewSheep(Sheep sheep) {
        this.sheepList.add(sheep);
    }

    public List<Cow> getCowList() {
        return cowList;
    }
    public void setNewCow(Cow cow) {
        this.cowList.add(cow);
    }

    public List<Chicken> getChickenList() {
        return chickenList;
    }
    public void setNewChicken(Chicken chicken) {
        this.chickenList.add(chicken);
    }

    public List<Horse> getHorseList() {
        return horseList;
    }
    public void setNewHorse(Horse horse) {
        this.horseList.add(horse);
    }

    public boolean isSheep() {
        return isSheep;
    }

    public void setSheep(boolean sheep) {
        isSheep = sheep;
    }

    public boolean isCow() {
        return isCow;
    }

    public void setCow(boolean cow) {
        isCow = cow;
    }

    public boolean isChicken() {
        return isChicken;
    }

    public void setChicken(boolean chicken) {
        isChicken = chicken;
    }

    public boolean isHorse() {
        return isHorse;
    }

    public void setHorse(boolean horse) { isHorse = horse; }

    public boolean isFeed() {
        return isFeed;
    }

    public void setFeed(boolean feed) {
        isFeed = feed;
    }

    public Farm() { }

    public double getMoney() { return money; }

    public Cow getCow() {
        return cow;
    }

    public Chicken getChicken() {
        return chicken;
    }

    public Sheep getSheep() {
        return sheep;
    }

    public Horse getHorse() {
        return horse;
    }

    public void setCow(Cow cow) { this.cow = cow; }
    public void setChicken(Chicken chicken) {
        this.chicken = chicken;
    }
    public void setSheep(Sheep sheep) {
        this.sheep = sheep;
    }
    public void setHorse(Horse horse) {
        this.horse = horse;
    }

    public void printHunger(int hungerValue) {
        System.out.print(hungerValue);
    }

    public double calculateTotalMoney() {
        int totalWool = 0, totalMilk = 0, totalEggs = 0;
        Double totalMoney;
        for (Chicken chickenElem : this.getChickenList())
            totalEggs += chickenElem.getEggs();
        for (Cow cowElem : this.getCowList())
            totalMilk += cowElem.getMilk();
        for (Sheep sheepElem : this.getSheepList())
            totalWool += sheepElem.getWool();
        totalMoney = money + totalEggs * eggsPrice + totalMilk * milkPrice + totalWool * woolPrice;
        return totalMoney;
    }
    public void showResources() {
        int totalWool = 0, totalMilk = 0, totalEggs = 0;
        for (Chicken chickenElem : this.getChickenList())
            totalEggs += chickenElem.getEggs();
        for (Cow cowElem : this.getCowList())
            totalMilk += cowElem.getMilk();
        for (Sheep sheepElem : this.getSheepList())
            totalWool += sheepElem.getWool();
        System.out.println("Ressourcen:");
        System.out.println("Geld ($" + money + ") || Geld nach Verkauf ($" + calculateTotalMoney() + ") || Wolle (" + totalWool + ") || Milch (" + totalMilk + ") || Eier (" + totalEggs + ")");
    }
    public void chooseAnimalToBuy() {
        boolean isValidInput = false;
        System.out.println("\nSie haben $" + calculateTotalMoney() + ". Welches Tier möchten Sie kaufen: ");
        System.out.println("[Schaf] $" + sheepPrice + " || [Kuh] $" + cowPrice + " || [Huhn] $" + chickenPrice + " || [Pferd] $" + horsePrice);
        String animalType = "Error";
        while (!isValidInput) {
            String userInput = scanner.next();

            try {

                animalType = userInput;

                if (userInput.toLowerCase().equals("schaf") || userInput.toLowerCase().equals("kuh") || userInput.toLowerCase().equals("huhn") || userInput.toLowerCase().equals("pferd")) {
                    isValidInput = true;
                } else {
                    System.out.println("Bitte Schaf, Kuh, Huhn oder Pferd eingeben:");
                }
            } catch (NumberFormatException e) {
                System.out.println("Bitte Schaf, Kuh, Huhn oder Pferd eingeben:");
            }
        }
        switch (animalType) {
            case "Schaf", "schaf":
                if(calculateTotalMoney() < this.sheepPrice) {
                    System.out.println("Sie haben leider nicht genug Geld!");
                    break;
                } else if (money < this.sheepPrice) {
                    System.out.println("Sie müssen erst Ihre Resourcen verkaufen, damit Sie genug Geld haben!");
                }else {
                    buyAnimal(animalType);
                    createNewAnimal(animalType);
                }
                break;
            case "Kuh", "kuh":
                if(calculateTotalMoney() < this.cowPrice) {
                    System.out.println("Sie haben leider nicht genug Geld!");
                    break;
                } else if (money < this.cowPrice) {
                    System.out.println("Sie müssen erst Ihre Resourcen verkaufen, damit Sie genug Geld haben!");
                }else {
                    buyAnimal(animalType);
                    createNewAnimal(animalType);
                }
                break;
            case "Huhn", "huhn":
                if(calculateTotalMoney() < this.chickenPrice) {
                    System.out.println("Sie haben leider nicht genug Geld!");
                    break;
                } else if (money < this.chickenPrice) {
                    System.out.println("Sie müssen erst Ihre Resourcen verkaufen, damit Sie genug Geld haben!");
                } else {
                    buyAnimal(animalType);
                    createNewAnimal(animalType);
                }
                break;
            case "Pferd", "pferd":
                if(calculateTotalMoney() < this.horsePrice) {
                    System.out.println("Sie haben leider nicht genug Geld!");
                    break;
                } else if (money < this.horsePrice) {
                    System.out.println("Sie müssen erst Ihre Resourcen verkaufen, damit Sie genug Geld haben!");
                } else {
                    buyAnimal(animalType);
                    createNewAnimal(animalType);
                }
                break;
        }
    }

    public void buyAnimal (String animalType) {
        switch (animalType) {
            case "Schaf", "schaf":
                money -= sheepPrice;
                break;
            case "Kuh", "kuh":
                money -= cowPrice;
                break;
            case "Huhn", "huhn":
                money -= chickenPrice;
                break;
            case "Pferd", "pferd":
                money -= horsePrice;
                break;
        }
    }
    public void resourcesToMoney() {
        money = calculateTotalMoney();
        for (Chicken chickenElem : this.getChickenList())
            chickenElem.setEggs(0);
        for (Cow cowElem : this.getCowList())
            cowElem.setMilk(0);
        for (Sheep sheepElem : this.getSheepList())
            sheepElem.setWool(0);
    }
    public void createNewAnimal(String animalType) {
        String animalName = "Error";
        String userInput = "Error";
        switch (animalType) {
            case "Schaf", "schaf":
                System.out.println("Wie soll das neue Schaf heißen:");
                userInput = scanner.next();
                System.out.println("Das neue Schaf heißt: " + userInput);
                Sheep sheep = new Sheep(animalName, 30, 3, 0);
                this.setNewSheep(sheep);
                break;
            case "Kuh", "kuh":
                System.out.println("Wie soll die neue Kuh heißen:");
                userInput = scanner.next();
                System.out.println("Die neue Kuh heißt: " + userInput);
                Cow cow = new Cow(animalName, 30, 3, 0);
                this.setNewCow(cow);
                break;
            case "Huhn", "huhn":
                System.out.println("Wie soll das neue Huhn heißen:");
                userInput = scanner.next();
                System.out.println("Das neue Huhn heißt: " + userInput);
                Chicken chicken = new Chicken(animalName, 30, 2, 0);
                this.setNewChicken(chicken);
                break;
            case "Pferd", "pferd":
                System.out.println("Wie soll das neue Pferd heißen:");
                userInput = scanner.next();
                System.out.println("Das neue Pferd heißt: " + userInput);
                Horse horse = new Horse(animalName, 30, 3);
                this.setNewHorse(horse);
                break;
        }
    }

    public int getHungerForAnimalInList(String animal) {
        isAllAnimals = false;
        boolean isValidInput = false;
        switch (animal) {
            case "Schaf", "schaf":
                isSheep = true;
                System.out.println("Sie können die Schafe wie folgt füttern:");
                System.out.print("[0] Alle || ");
                for (int i = 1; i <= getSheepList().size(); i++) {
                    System.out.print("[" + i + "] Schaf #" + i + ": ");
                    printHunger(getSheepList().get(i-1).getHunger());
                    System.out.print(" || ");
                }
                System.out.println("");
                while (!isValidInput) {
                    String userInput = scanner.next();

                    try {
                        animalInList = Integer.parseInt(userInput);

                        if ( animalInList >= 0 && animalInList <= getSheepList().size()) {
                            isValidInput = true;
                        } else {
                            System.out.println("Bitte eine Zahl von 0 bis " + getSheepList().size() + " eingeben:");
                        }
                    } catch (NumberFormatException e) {
                        System.out.println("Bitte eine Zahl von 0 bis " + getSheepList().size() + " eingeben:");
                    }
                }
                if(animalInList == 0){
                    isAllAnimals = true;
                    return 50;
                }
                return getSheepList().get(animalInList-1).getHunger();
            case "Kuh", "kuh":
                isCow = true;
                System.out.println("Sie können die Kühe wie folgt füttern:");
                System.out.print("[0] Alle || ");
                for (int i = 1; i <= getCowList().size(); i++) {
                    System.out.print("[" + i + "] Kuh #" + i + ": ");
                    printHunger(getCowList().get(i-1).getHunger());
                    System.out.print(" || ");
                }
                System.out.println("");
                while (!isValidInput) {
                    String userInput = scanner.next();

                    try {
                        animalInList = Integer.parseInt(userInput);

                        if ( animalInList >= 0 && animalInList <= getCowList().size()) {
                            isValidInput = true;
                        } else {
                            System.out.println("Bitte eine Zahl von 0 bis " + getCowList().size() + " eingeben:");
                        }
                    } catch (NumberFormatException e) {
                        System.out.println("Bitte eine Zahl von 0 bis " + getCowList().size() + " eingeben:");
                    }
                }
                if(animalInList == 0){
                    isAllAnimals = true;
                    return 50;
                }
                return getCowList().get(animalInList-1).getHunger();
            case "Huhn", "huhn":
                isChicken = true;
                System.out.println("Sie können die Hühner wie folgt füttern:");
                System.out.print("[0] Alle || ");
                for (int i = 1; i <= getChickenList().size(); i++) {
                    System.out.print("[" + i + "] Huhn #" + i + ": ");
                    printHunger(getChickenList().get(i-1).getHunger());
                    System.out.print(" || ");
                }
                System.out.println("");
                while (!isValidInput) {
                    String userInput = scanner.next();

                    try {
                        animalInList = Integer.parseInt(userInput);

                        if ( animalInList >= 0 && animalInList <= getChickenList().size()) {
                            isValidInput = true;
                        } else {
                            System.out.println("Bitte eine Zahl von 0 bis " + getChickenList().size() + " eingeben:");
                        }
                    } catch (NumberFormatException e) {
                        System.out.println("Bitte eine Zahl von 0 bis " + getChickenList().size() + " eingeben:");
                    }
                }
                if(animalInList == 0){
                    isAllAnimals = true;
                    return 50;
                }
                return getChickenList().get(animalInList-1).getHunger();
            case "Pferd", "pferd":
                isHorse = true;
                System.out.println("Sie können die Pferde wie folgt füttern:");
                System.out.print("[0] Alle || ");
                for (int i = 1; i <= getHorseList().size(); i++) {
                    System.out.print("[" + i + "] Kuh #" + i + ": ");
                    printHunger(getHorseList().get(i-1).getHunger());
                    System.out.print(" || ");
                }
                System.out.println("");
                while (!isValidInput) {
                    String userInput = scanner.next();

                    try {
                        animalInList = Integer.parseInt(userInput);

                        if ( animalInList >= 0 && animalInList <= getHorseList().size()) {
                            isValidInput = true;
                        } else {
                            System.out.println("Bitte eine Zahl von 0 bis " + getHorseList().size() + " eingeben:");
                        }
                    } catch (NumberFormatException e) {
                        System.out.println("Bitte eine Zahl von 0 bis " + getHorseList().size() + " eingeben:");
                    }
                }
                if(animalInList == 0){
                    isAllAnimals = true;
                    return 50;
                }
                return getHorseList().get(animalInList-1).getHunger();
            default:
                System.out.println("Kein passendes Tier gefunden!");
                return -1;
        }
    }


    public void hunger() {
        if (this.isChicken()) {
            this.getCowList().forEach(cowElem -> { cowElem.setHunger(cowElem.hunger()); });
            this.getSheepList().forEach(sheepElem -> { sheepElem.setHunger(sheepElem.hunger()); });
            this.getHorseList().forEach(horseElem -> { horseElem.setHunger(horseElem.hunger()); });
            if(animalInList!=0) {
                this.getChickenList().forEach(chickenElem -> {
                    if (this.getChickenList().get(animalInList - 1) == chickenElem) {
                    } else {
                        chickenElem.setHunger(chickenElem.hunger());
                    }
                });
            }
        } else if (this.isSheep()) {
            this.getChickenList().forEach(chickenElem -> { chickenElem.setHunger(chickenElem.hunger()); });
            this.getCowList().forEach(cowElem -> { cowElem.setHunger(cowElem.hunger()); });
            this.getHorseList().forEach(horseElem -> { horseElem.setHunger(horseElem.hunger()); });
            if(animalInList!=0) {
                this.getSheepList().forEach(sheepElem -> {
                    if (this.getSheepList().get(animalInList - 1) == sheepElem) {
                    } else {
                        sheepElem.setHunger(sheepElem.hunger());
                    }
                });
            }
        } else if (this.isCow()) {
            this.getSheepList().forEach(sheepElem -> { sheepElem.setHunger(sheepElem.hunger()); });
            this.getChickenList().forEach(chickenElem -> { chickenElem.setHunger(chickenElem.hunger()); });
            this.getHorseList().forEach(horseElem -> { horseElem.setHunger(horseElem.hunger()); });
            if(animalInList!=0) {
                this.getCowList().forEach(cowElem -> {
                    if (this.getCowList().get(animalInList - 1) == cowElem) {
                    } else {
                        cowElem.setHunger(cowElem.hunger());
                    }
                });
            }
        } else if (this.isHorse()) {
            this.getSheepList().forEach(sheepElem -> { sheepElem.setHunger(sheepElem.hunger()); });
            this.getChickenList().forEach(chickenElem -> { chickenElem.setHunger(chickenElem.hunger()); });
            this.getCowList().forEach(cowElem -> { cowElem.setHunger(cowElem.hunger()); });
            if (animalInList != 0) {
                this.getHorseList().forEach(horseElem -> {
                    if (this.getHorseList().get(animalInList - 1) == horseElem) {
                    } else {
                        horseElem.setHunger(horseElem.hunger());
                    }
                });
            }
        }
    }
    public void births() {
        double chickenBirthProb = 0.1;
        double horseBirthProb = 0.08;
        double sheepBirthProb = 0.05;
        double cowBirthProb = 0.03;

        int i, birthCnt;
        birthCnt = 0;
        for(i = 0; i < this.getChickenList().size() - birthCnt; i++) {
            double rand = Math.random();
            if (rand <= chickenBirthProb) {
                String name = this.getChickenList().get(i).getName();
                String newName = name + "Baby";
                Chicken chicken = new Chicken(newName, 30, 1, 1);
                this.setNewChicken(chicken);
                birthCnt++;
                int j = i+1;
                System.out.println("Huhn #" + j + " hat ein neues Küken geboren. Ihre Farm wächst!");
            }
        }
        birthCnt = 0;
        for(i = 0; i < this.getSheepList().size() - birthCnt; i++) {
            double rand = Math.random();
            if (rand <= sheepBirthProb) {
                String name = this.getSheepList().get(i).getName();
                String newName = name + "Baby";
                Sheep sheep = new Sheep(newName, 30, 1, 1);
                this.setNewSheep(sheep);
                birthCnt++;
                int j = i+1;
                System.out.println("Schaf #" + j + " hat ein neues Lamm geboren. Ihre Farm wächst!");
            }
        }
        birthCnt = 0;
        for(i = 0; i < this.getCowList().size() - birthCnt; i++) {
            double rand = Math.random();
            if (rand <= cowBirthProb) {
                String name = this.getCowList().get(i).getName();
                String newName = name + "Baby";
                Cow cow = new Cow(newName, 30, 1, 1);
                this.setNewCow(cow);
                birthCnt++;
                int j = i+1;
                System.out.println("Kuh #" + j + " hat ein neues Kalb geboren. Ihre Farm wächst!");
            }
        }
        birthCnt = 0;
        for(i = 0; i < this.getHorseList().size() - birthCnt; i++) {
            double rand = Math.random();
            if (rand <= horseBirthProb) {
                String name = this.getHorseList().get(i).getName();
                String newName = name + "Baby";
                Horse horse = new Horse(newName, 30, 1);
                this.setNewHorse(horse);
                birthCnt++;
                int j = i+1;
                System.out.println("Kuh #" + j + " hat ein neues Fohlen geboren. Ihre Farm wächst!");
            }
        }
    }
    public void horseRace() {
        int numHorses = this.getHorseList().size();
        double horseMoneyWin = 75;
        money += horseMoneyWin * numHorses;
    }
}
