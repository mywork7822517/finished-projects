import animals.Chicken;
import animals.Cow;
import animals.Sheep;
import farm.Farm;

import java.util.Random;
import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);
    static Farm farm;
    static int round = 1;
    static double points = 0;
    static double pointsLastRound = 0;
    static int hunger;
    static boolean sheeps = false;
    static boolean cows = false;
    static boolean chickens = false;
    static boolean horses = false;

    public static void main(String[] args) { virtualFarmGame(); }

    public static void virtualFarmGame() {

        sheeps = false;
        cows = false;
        chickens = false;
        horses = false;
        gameIntro();


        hunger = 0;

        Sheep sheep = new Sheep("blacksheep", 50, 3, 2);
        Cow cow = new Cow("muh", 60, 6, 1);
        Chicken chicken = new Chicken("chick1", 40, 1, 4);
        Chicken chicken2 = new Chicken("chick2", 40, 2, 6);
        Chicken chicken3 = new Chicken("chick3", 40, 3, 8);

        farm = new Farm();

        farm.setNewSheep(sheep);
        farm.setNewCow(cow);
        farm.setNewChicken(chicken);
        farm.setNewChicken(chicken2);
        farm.setNewChicken(chicken3);

        while (true) {

            farm.getSheepList().forEach(sheepElem -> { farm.setSheep(false); });
            farm.getCowList().forEach(cowElem -> { farm.setCow(false); });
            farm.getSheepList().forEach(chickenElem -> { farm.setChicken(false); });
            farm.getHorseList().forEach(horseElem -> { farm.setHorse(false); });
            farm.setFeed(false);

            rounds();
            printHunger();
            farm.showResources();
            if (gameRoute()) {

                if (hunger < 0)
                    continue;

                if (isHungry(hunger)) {
                    printNewHunger();
                } else {
                    continue;
                }

                if (hunger < 0)
                    hunger = 0;

                farm.horseRace();
                farm.hunger(); //increase hunger every round
                farm.births();;
                wolf();
                agingAndDying();
                if (gameOver()) {
                    System.out.println("Sie haben " + points() + " Punkte erreicht. Glückwunsch!");
                    break;
                }

                round++;
            }
        }
    }

    public static String chooseAnimal() {
        boolean isOkInput = false;
        String newAnimal = "Error";
        System.out.println("\n\nWelches Tier möchten Sie füttern: ");
        System.out.println("[Schaf] (" + farm.getSheepList().size() + ") || [Kuh] (" + farm.getCowList().size() + ") || [Huhn] (" + farm.getChickenList().size() + ") || [Pferd] (" + farm.getHorseList().size() + ")");
        while (!isOkInput) {
            String userInput = scanner.next();

            try {

                newAnimal = userInput;

                if (userInput.toLowerCase().equals("schaf") || userInput.toLowerCase().equals("kuh") || userInput.toLowerCase().equals("huhn") || userInput.toLowerCase().equals("pferd")) {
                    isOkInput = true;
                } else {
                    System.out.println("Bitte Schaf, Kuh, Huhn oder Pferd eingeben:");
                }
            } catch (NumberFormatException e) {
                System.out.println("Bitte Schaf, Kuh, Huhn oder Pferd eingeben:");
            }
        }

        switch (newAnimal) {
            case "Schaf", "schaf":
                if (farm.getSheepList().size() == 0) {
                    System.out.println("Sie besitzen keine Schafe.");
                    chooseAnimal();
                }
                break;
            case "Huhn", "huhn":
                if (farm.getChickenList().size() == 0) {
                    System.out.println("Sie besitzen keine Hühner.");
                    chooseAnimal();
                }
                break;
            case "Kuh", "kuh":
                if (farm.getCowList().size() == 0) {
                    System.out.println("Sie besitzen keine Kühe.");
                    chooseAnimal();
                }
                break;
            case "Pferd", "pferd":
                if (farm.getHorseList().size() == 0) {
                    System.out.println("Sie besitzen keine Pferde.");
                    chooseAnimal();
                }
                break;
        }
        return newAnimal;
    }

    public static boolean isHungry(int hunger) {
        if (hunger >= 75) {
            if(farm.isAllAnimals)
                System.out.println("Die Tiere werden gefüttert.");
            else
                System.out.println("Das Tier wird gefüttert.");
            return true;
        } else if (hunger >= 50 && hunger < 75) {
            while (true) {
                if(farm.isAllAnimals)
                    System.out.println("Sollen die Tiere gefüttert werden? [J/N].");
                else
                    System.out.println("Soll das Tier gefüttert werden? [J/N]");
                String hungerFeed = scanner.next();
                if (hungerFeed.toLowerCase().equals("j")) {
                    return true;
                } else if (hungerFeed.toLowerCase().equals("n")) {
                    return false;
                } else {
                    if(farm.isAllAnimals)
                        System.out.println("Inkorrekte Eingabe. Sollen die Tiere gefüttert werden? [J/N]");
                    else
                        System.out.println("Inkorrekte Eingabe. Soll das Tier gefüttert werden? [J/N]");
                }
            }
        } else {
            System.out.println("Das Tier kann nicht gefüttert werden.");
            return false;
        }
    }

    public static void printHunger() {
        System.out.println("Hungerwerte:");
        for (int i = 1; i <= farm.getSheepList().size(); i++) {
            System.out.print("Schaf #" + i + ": ");
            farm.printHunger(farm.getSheepList().get(i-1).getHunger());
            System.out.print(" || ");
        }
        System.out.println("");
        for (int i = 1; i <= farm.getCowList().size(); i++) {
            System.out.print("Kuh #" + i + ":   ");
            farm.printHunger(farm.getCowList().get(i-1).getHunger());
            System.out.print(" || ");
        }
        System.out.println("");
        for (int i = 1; i <= farm.getChickenList().size(); i++) {
            System.out.print("Huhn #" + i + ":  ");
            farm.printHunger(farm.getChickenList().get(i-1).getHunger());
            System.out.print(" || ");
        }
        System.out.println("");
        for (int i = 1; i <= farm.getHorseList().size(); i++) {
            System.out.print("Pferd #" + i + ": ");
            farm.printHunger(farm.getHorseList().get(i-1).getHunger());
            System.out.print(" || ");
        }
    }

    public static void printNewHunger() {
        if (farm.isSheep()) {
            if(farm.animalInList != 0){
                farm.getSheepList().get(farm.animalInList-1).setHunger((farm.getSheepList().get(farm.animalInList-1).feed()));
                System.out.println("Der neue Hungerwert von Schaf #" + farm.animalInList + " beträgt: " + farm.getSheepList().get(farm.animalInList-1).getHunger());
            } else {
                farm.getSheepList().forEach(sheepElem -> { sheepElem.feedAll(); });
            }
        } else if (farm.isCow()) {
            if(farm.animalInList != 0){
                farm.getCowList().get(farm.animalInList-1).setHunger((farm.getCowList().get(farm.animalInList-1).feed()));
                System.out.println("Der neue Hungerwert von Kuh #" + farm.animalInList + " beträgt: " + farm.getCowList().get(farm.animalInList-1).getHunger());
            } else {
                farm.getCowList().forEach(cowElem -> { cowElem.feedAll(); });
            }
        } else if (farm.isChicken()) {
            if(farm.animalInList != 0){
                farm.getChickenList().get(farm.animalInList-1).setHunger((farm.getChickenList().get(farm.animalInList-1).feed()));
                System.out.println("Der neue Hungerwert von Huhn #" + farm.animalInList + " beträgt: " + farm.getChickenList().get(farm.animalInList-1).getHunger());
            } else {
                farm.getChickenList().forEach(chickenElem -> { chickenElem.feedAll(); });
            }
        } else if (farm.isHorse()) {
            if(farm.animalInList != 0){
                farm.getHorseList().get(farm.animalInList-1).setHunger((farm.getHorseList().get(farm.animalInList-1).feed()));
                System.out.println("Der neue Hungerwert von Pferd #" + farm.animalInList + " beträgt: " + farm.getHorseList().get(farm.animalInList-1).getHunger());
            } else {
                farm.getHorseList().forEach(horseElem -> { horseElem.feedAll(); });
            }
        }
    }

    public static void printAge() {
        System.out.println("");
        System.out.println("Durchschnittliche Lebenserwartung der Tiere: ");
        System.out.println("Schaf: 11 Jahre || Huhn: 7 Jahre || Kuh: 9 Jahre || Pferd: 12 Jahre");
        System.out.println("1 Jahr ist vergangen... Neues Alter der Tiere:");
        for (int i = 1; i <= farm.getSheepList().size(); i++) {
            System.out.print("Schaf #" + i + ": ");
            farm.printHunger(farm.getSheepList().get(i-1).getAge());
            System.out.print(" || ");
        }
        System.out.println("");
        for (int i = 1; i <= farm.getCowList().size(); i++) {
            System.out.print("Kuh #" + i + ":   ");
            farm.printHunger(farm.getCowList().get(i-1).getAge());
            System.out.print(" || ");
        }
        System.out.println("");
        for (int i = 1; i <= farm.getChickenList().size(); i++) {
            System.out.print("Huhn #" + i + ":  ");
            farm.printHunger(farm.getChickenList().get(i-1).getAge());
            System.out.print(" || ");
        }
        System.out.println("");
        for (int i = 1; i <= farm.getHorseList().size(); i++) {
            System.out.print("Pferd #" + i + ": ");
            farm.printHunger(farm.getHorseList().get(i-1).getAge());
            System.out.print(" || ");
        }
        System.out.println("");
    }
    public static void agingAndDying(){
        Random rand = new Random();

        if(round % 3 == 0) {
            farm.getChickenList().forEach(chickenElem -> chickenElem.setAge(chickenElem.aging()));
            farm.getCowList().forEach(cowElem -> cowElem.setAge(cowElem.aging()));
            farm.getSheepList().forEach(sheepElem -> sheepElem.setAge(sheepElem.aging()));
            farm.getHorseList().forEach(horseElem -> horseElem.setAge(horseElem.aging()));
            printAge();
        }

        for (int i = 1; i <= farm.getChickenList().size(); i++) {
            if (farm.getChickenList().get(i-1).getAge() >= (rand.nextInt(5) + 5)) { //chickenLifeExptectancy
                System.out.println("Huhn #" + i + " ist an Alter gestorben.");
                farm.getChickenList().remove(i-1);
            }
        }
        for (int i = 1; i <= farm.getCowList().size(); i++) {
            if (farm.getCowList().get(i-1).getAge() >= (rand.nextInt(5) + 7)) { //cowLifeExptectancy
                System.out.println("Kuh #" + i + " ist an Alter gestorben.");
                farm.getCowList().remove(i-1);
            }
        }
        for (int i = 1; i <= farm.getSheepList().size(); i++) {
            if (farm.getSheepList().get(i-1).getAge() >= (rand.nextInt(3) + 10)) { //sheepLifeExptectancy
                System.out.println("Schaf #" + i + " ist an Alter gestorben.");
                farm.getSheepList().remove(i-1);
            }
        }
        for (int i = 1; i <= farm.getHorseList().size(); i++) {
            if (farm.getHorseList().get(i-1).getAge() >= (rand.nextInt(5) + 10)) { //horseLifeExptectancy
                System.out.println("Pferd #" + i + " ist an Alter gestorben.");
                farm.getHorseList().remove(i-1);
            }
        }
    }

    public static void wolf() {
        double rand = Math.random();
        double wolfChickenProb = 0.1;
        double wolfSheepProb = 0.05;
        double wolfCowProb = 0.01;
        double cascadeProb = 0.5;

        if(rand <= wolfChickenProb) {
            farm.getChickenList().remove(0);
            int i;
            for (i = 0; i < farm.getChickenList().size(); i++) {
                double randCascade = Math.random();
                if(randCascade <= cascadeProb) {
                    farm.getChickenList().remove(i);
                } else {
                    break;
                }
            }
            if(i == 0)
                System.out.println("Der Wolf hat diese Nacht 1 Huhn getötet.");
            else
                System.out.println("Der Wolf hat diese Nacht " + (i+1) + " Hühner getötet.");
        }
        if(rand <= wolfSheepProb) {
            farm.getChickenList().remove(0);
            int i;
            for (i = 0; i < farm.getSheepList().size(); i++) {
                double randCascade = Math.random();
                if(randCascade <= cascadeProb) {
                    farm.getSheepList().remove(i);
                } else {
                    break;
                }
            }
            if(i == 0)
                System.out.println("Der Wolf hat diese Nacht 1 Schaf getötet.");
            else {
                System.out.println("Der Wolf hat diese Nacht " + (i+1) + " Schafe getötet.");
            }
        }
        if(rand <= wolfCowProb) {
            farm.getCowList().remove(0);
            int i;
            for (i = 0; i < farm.getCowList().size(); i++) {
                double randCascade = Math.random();
                if(randCascade <= cascadeProb) {
                    farm.getCowList().remove(i);
                } else {
                    break;
                }
            }
            if(i == 0)
                System.out.println("Der Wolf hat diese Nacht 1 Kuh getötet.");
            else
                System.out.println("Der Wolf hat diese Nacht " + (i+1) + " Kühe getötet.");
        }
    }

    public static void gameIntro() {
        while (true) {
            System.out.println("Möchten Sie die Spielregeln erklärt bekommen? [J/N]");
            String introQuery = scanner.next();
            if (introQuery.toLowerCase().equals("j")) {
                StringBuilder intro = new StringBuilder();
                intro.append("Willkommen in der Virtual Farm!\nSie sind Bauer und hüten über 4 verschiedene Arten von Tiere:\n");
                intro.append("Schafe, Hühner, Kühe und Pferde.\n");
                intro.append("Ziel ist es ihre Farm zum Wachsen zu bringen. Nutztiere erzeugen Rohstoffe, die Sie verkaufen können, und Pferde bringen jede Runde Geld mittels Pferderennen.\n");
                intro.append("Das Geld können Sie benutzen, um weitere Tiere einzukaufen und somit Ihre Farm auszubauen.\n");
                intro.append("Doch Achtung! Sind Sie nachlässig oder unvorsichtig, können Ihre Tiere verhungern, an Alter sterben oder vom nächtlichen Wolf gefressen werden.\n");
                intro.append("Das Spiel ist vorbei, wenn sämtliche Tiere einer Art um's Leben gekommen sind.\n");
                intro.append("Drücken Sie Enter, wenn Sie bereit sind...");
                String output = intro.toString();
                System.out.println(output);
                Scanner sc = new Scanner(System.in);
                sc.nextLine();
                System.out.println("Viel Spaß. Ich zähle die Punkte!\n");
                break;
            } else if (introQuery.toLowerCase().equals("n")) {
                break;
            } else
                System.out.println("Inkorrekte Eingabe. Möchten Sie die Spielregen erklärt bekommen? [J/N]");
        }
    }

    public static boolean gameRoute() {
        boolean isValidInput = false;
        int choice = 0;
        System.out.println("\n");
        System.out.println("Was möchten Sie tun:");
        while (!isValidInput) {
            System.out.println("[1] Tiere füttern || [2] Tiere kaufen || [3] Tiere anzeigen || [4] Ressourcen verkaufen || [5] Ressourcen anzeigen");
            String userInput = scanner.next();

            try {
                choice = Integer.parseInt(userInput);

                if ( choice >= 1 && choice <= 5) {
                    isValidInput = true;
                } else {
                    System.out.println("Bitte eine Zahl von 1 bis 5 eingeben:");
                }
            } catch (NumberFormatException e) {
                System.out.println("Bitte eine Zahl von 1 bis 5 eingeben:");
            }
        }
        switch (choice) {
            case 1:
                printHunger();
                hunger = farm.getHungerForAnimalInList(chooseAnimal());
                return true;
            case 2:
                farm.chooseAnimalToBuy();
                return gameRoute();
            case 3:
                printHunger();
                return gameRoute();
            case 4:
                farm.resourcesToMoney();
                farm.showResources();
                return gameRoute();
            case 5:
                farm.showResources();
                return gameRoute();
            default:
                System.out.println("Error bei gameRoute!");
                return false;
        }
    }

    public static void rounds() {
        points = points();
        double pointsGained = points - pointsLastRound;
        if(round > 1)
            System.out.println("");
        System.out.println("------------");
        System.out.println("| ROUND " + round + ": |");
        System.out.println("------------");
        System.out.println("Punkte: " + points + " (+" + pointsGained + ")");
        System.out.println("");
        pointsLastRound = points;
    }
    public static boolean gameOver() {
        if(farm.getChickenList().size() > 0) //check ob Liste schon mal angelegt war
            chickens = true;
        if(farm.getSheepList().size() > 0)
            sheeps = true;
        if(farm.getCowList().size() > 0)
            cows = true;
        if(farm.getHorseList().size() > 0)
            horses = true;

        farm.getSheepList().forEach(sheepElem -> {
            if (sheepElem.getHunger() > 100) {
                System.out.println("Ein Schaf ist verhungert: " + sheepElem.toString());
                farm.getSheepList().remove(sheepElem);
            }
        });

        farm.getCowList().forEach(cowElem -> {
            if (cowElem.getHunger() > 100) {
                System.out.println("Ein Schaf ist verhungert: " + cowElem.toString());
                farm.getCowList().remove(cowElem);
            }
        });

        farm.getChickenList().forEach(chickenElem -> {
            if (chickenElem.getHunger() > 100) {
                System.out.println("Ein Schaf ist verhungert: " + chickenElem.toString());
                farm.getChickenList().remove(chickenElem);
            }
        });

        farm.getHorseList().forEach(horseElem -> {
            if (horseElem.getHunger() > 100) {
                System.out.println("Ein Pferd ist verhungert: " + horseElem.toString());
                farm.getChickenList().remove(horseElem);
            }
        });

        if(farm.getChickenList().size() == 0 || farm.getCowList().size() == 0 || farm.getSheepList().size() == 0 || farm.getHorseList().size() == 0)

        if(sheeps) {
            if (farm.getSheepList().size() == 0) {
                System.out.println("Alle Schafe sind gestorben. Das Spiel ist damit vorbei.");
                return true;
            } else {
                return false;
            }
        }
        if(cows) {
            if (farm.getCowList().size() == 0) {
                System.out.println("Alle Kühe sind gestorben. Das Spiel ist damit vorbei.");
                return true;
            } else {
                return false;
            }
        }
        if(chickens) {
            if (farm.getChickenList().size() == 0) {
                System.out.println("Alle Hühner sind gestorben. Das Spiel ist damit vorbei.");
                return true;
            } else {
                return false;
            }
        }
        if(horses) {
            if (farm.getHorseList().size() == 0) {
                System.out.println("Alle Pferde sind gestorben. Das Spiel ist damit vorbei.");
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
    public static double points() {
        double points;
        double chickenFactor = farm.chickenPrice;
        double sheepFactor = farm.sheepPrice;
        double cowFactor = farm.cowPrice;
        double horseFactor = farm.horsePrice;
        double roundsFactor = 10;
        double chickenBonus = 0;
        double sheepBonus = 0;
        double cowBonus = 0;
        double horseBonus = 0;
        int numChickens = farm.getChickenList().size();
        int numSheeps = farm.getSheepList().size();
        int numCows = farm.getCowList().size();
        int numHorses = farm.getHorseList().size();

        if(numChickens > 0)
            chickenBonus = 10;
        if(numCows > 0)
            cowBonus = 10;
        if(numSheeps > 0)
            sheepBonus = 10;
        if(numHorses > 0)
            horseBonus = 50;
        points = farm.calculateTotalMoney() + round * roundsFactor + numChickens * chickenFactor + numSheeps * sheepFactor + numCows * cowFactor + numHorses * horseFactor + chickenBonus + sheepBonus + cowBonus + horseBonus;
        return points;
    }
}