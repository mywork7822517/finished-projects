package animals;

public class Cow extends Animal {
    private int milk;

    public Cow(String name, int hunger, int age, int milk) {
        super(name, hunger, age);
        this.milk = milk;
    }

    public Cow() {
    }
    @Override
    public int feed() {
        milk+=2;
        return hunger -= 30;
    }
    @Override
    public int feedAll() {
        milk++;
        return hunger -= 10;
    }

    public String getName() {
        return name;
    }

    public int getHunger() {
        return hunger;
    }

    public int getAge() {
        return age;
    }

    public int getMilk() {
        return milk;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHunger(int hunger) {
        this.hunger = hunger;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setMilk(int milk) {
        this.milk = milk;
    }
}