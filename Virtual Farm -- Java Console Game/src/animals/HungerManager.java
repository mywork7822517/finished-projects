package animals;

public interface HungerManager {
    int hungerValue = 10;

    int feed();
    int feedAll();
    int hunger();
    int aging();
}