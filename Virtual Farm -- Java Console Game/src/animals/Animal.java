package animals;
public class Animal implements HungerManager {
    protected String name;
    protected int hunger;
    protected int age;

    public Animal(String name, int hunger, int age) {
        this.name = name;
        this.hunger = hunger;
        this.age = age;
    }

    public Animal() {
    }

    @Override
    public int feed() {
        hunger -= 50;
        return hunger;
    }
    public int feedAll() {
        hunger -= 20;
        return hunger;
    }
    @Override
    public int hunger() {
        hunger += hungerValue;
        return hunger;
    }
    public int aging() {
        age++;
        return age;
    }
}
