package animals;

public class Chicken extends Animal {
    private int eggs;

    public Chicken(String name, int hunger, int age, int eggs) {
        super(name, hunger, age);
        this.eggs = eggs;
    }

    public Chicken() {
    }
    @Override
    public int feed() {
        eggs+=6;
        return super.feed();
    }
    @Override
    public int feedAll() {
        eggs+=2;
        return super.feedAll();
    }

    public String getName() {
        return name;
    }

    public int getHunger() {
        return hunger;
    }

    public int getAge() {
        return age;
    }

    public int getEggs() {
        return eggs;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHunger(int hunger) {
        this.hunger = hunger;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setEggs(int eggs) {
        this.eggs = eggs;
    }
}