package animals;

public interface Market {
    int sheepPrice = 60; //2x wool
    int cowPrice = 100;
    int chickenPrice = 20;
    int horsePrice = 150;
    int woolPrice = 15;
    int milkPrice = 40;
    int eggsPrice = 5;

    double calculateTotalMoney();
    void showResources();
    void chooseAnimalToBuy();
    void buyAnimal(String animalType);
    void resourcesToMoney();
}
