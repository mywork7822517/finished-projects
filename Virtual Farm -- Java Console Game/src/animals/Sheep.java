package animals;

import java.util.ArrayList;
import java.util.List;

public class Sheep extends Animal {
    private int wool;
    public Sheep(String name, int hunger, int age, int wool) {
        super(name, hunger, age);
        this.wool = wool;
    }

    @Override
    public String toString() {
        return "Sheep" +
                ", name=" + name + '\'' +
                ", hunger=" + hunger +
                ", age=" + age +
                ", wool=" + wool;
    }

    public Sheep() {
    }

    @Override
    public int feed() {
        wool+=2;
        return super.feed();
    }
    @Override
    public int feedAll() {
        wool++;
        return super.feedAll();
    }

    public String getName() {
        return name;
    }

    public int getHunger() {
        return hunger;
    }

    public int getAge() {
        return age;
    }

    public int getWool() {
        return wool;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHunger(int hunger) {
        this.hunger = hunger;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setWool(int wool) {
        this.wool = wool;
    }
}
