package animals;

public class Horse extends Animal {

    public Horse(String name, int hunger, int age) {
        super(name, hunger, age);
    }

    public Horse() {
    }

    public int feed() {
        return super.feed();
    }

    public int feedAll() {
        return super.feedAll();
    }

    @Override
    public int hunger() {
        hunger += hungerValue/2;
        return hunger;
    }

    public String getName() {
        return name;
    }

    public int getHunger() {
        return hunger;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHunger(int hunger) {
        this.hunger = hunger;
    }

    public void setAge(int age) {
        this.age = age;
    }

}
