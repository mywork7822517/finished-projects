Virtual Farm is a game that you can play inside the Java Console.

Here's how it works:
You're the owner of a farm and can own up to 4 different species of animals: Sheeps, cows, chickens and horses.
Sheeps, cows and chickens produce resources you can sell at the market to make money, while horses earn you money via horse races.
You can spend your money to acquire new animals and grow your farm.
But beware!
Animals can also die from hunger, if you're negligent with feeding them, or be killed by the nightly wolf.
If all animals of one species are dead, the game is over.
Good luck, and I count the points!