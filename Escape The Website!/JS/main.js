const nameInput = document.getElementById('nameInput');
const squareContainer = document.getElementById('square-container');
const squareContainerContent = document.getElementById('square-container');
const inputLettersContainer = document.getElementById('inputLettersContainer');
const inputNumbersContainer = document.getElementById('inputNumbersContainer');
const nameOutput = document.getElementById('nameOutput');
const colorChanger = document.getElementById('colorChanger');
const colorRangeContainer = document.querySelector(".colorRangeContainer");
const buttonExplosionContainer = document.getElementById('buttonExplosionContainer');
const pointsDiv = document.getElementById('points');
const helpButton = document.getElementById('helpButton');
const enterSitePopup = document.getElementById('enterSite');
const enterSiteButtonContainer = document.getElementById('enterSiteButtonContainer');
const exitColor = document.getElementById('exit');
const lightsMenuItem = document.getElementById('lightsMenuItem');
const homeButton = document.getElementById('homeButton');
const inputLetters = document.querySelectorAll('.inputLetters');
const uiButtons = document.querySelectorAll('.uiButton');
const navbar = document.getElementById('navbar');
const menuItems = document.querySelectorAll('.menuItem');
const submenuItems = document.querySelectorAll('.submenuItems');
const squares = document.querySelectorAll('.square');
const bodyColorRange = document.getElementById('bodyColorRange');
const fontColorRange = document.getElementById('fontColorRange');
const buttonColorRange = document.getElementById('buttonColorRange');
const buttonFontColorRange = document.getElementById('buttonFontColorRange');
const keyboardColorRange = document.getElementById('keyboardColorRange');
const keyboardFontColorRange = document.getElementById('keyboardFontColorRange');
const menuBgColorRange = document.getElementById('menuBgColorRange');
const menuAccentColorRange = document.getElementById('menuAccentColorRange');
const menuFontColorRange = document.getElementById('menuFontColorRange');
const shiftGameColorRange = document.getElementById('shiftGameColorRange');
const shiftGameFontColorRange = document.getElementById('shiftGameFontColorRange');
const startButton = document.getElementById('startButton');
const saveButton = document.getElementById('saveButton');
const enterWebsiteSection = document.getElementById('enterWebsite');
const enterNameSection = document.getElementById('enterName');
const gameStartSection = document.getElementById('gameStart');
const key = document.getElementById('key');
const keySubmitButton = document.getElementById('keySubmitButton');
const inputLettersContainerNoFlex = document.getElementById('inputLettersContainerNoFlex');
const hideGame = document.getElementById('hideGame');
const hideEnterKeyContainer = document.getElementById('enterKeyContainer');

let body = document.body;

const squareButtonsArray = Array.from(document.querySelectorAll(".square")); //winning game state
let squareButtonsArrayCopy = [...squareButtonsArray];   //copies to change game states
let squareButtonsArrayChange = [...squareButtonsArray];

let r = 0, g = 0, b = 0;
let heroName = '';
let keyInput ='';
let points = 42;
let helpCnt = 0;
let lastColorChanger;

/* enter site */
enterSiteButtonContainer.addEventListener("click", x => {
    if(x.target.innerHTML === "No")
        enterSitePopup.classList.add('hidden');
    sessionStorage.setItem("enterWebsite","yes");
});

/* reload conditions */
if(sessionStorage.getItem("enterWebsite") === 'yes') {
    enterSitePopup.classList.add('hidden');
    document.getElementById('lights').classList.add('hidden');
    pointsDiv.classList.remove('hidden');
}

/* initial dialogue */
startButton.addEventListener("click", x => {
    enterWebsiteSection.classList.add('none');
    enterNameSection.classList.remove('none');
    inputLettersContainerNoFlex.classList.remove('none');
});
saveButton.addEventListener("click", e => {
    enterNameSection.classList.add('none');
    gameStartSection.classList.remove('none');
    inputLettersContainerNoFlex.classList.add('none');
});

/* website color menu popup */
exitColor.addEventListener("click", x => {
    document.getElementById('lights').classList.add('hidden');
    pointsDiv.classList.remove('hidden');
});

lightsMenuItem.addEventListener("click", x => {
    document.getElementById('lights').classList.remove('hidden');
    pointsDiv.classList.add('hidden');
});
homeButton.addEventListener("click", x => {
    enterSitePopup.classList.remove('hidden');
});


/* website color changers */
colorRangeContainer.addEventListener("change", rangeToColor);
bodyColorRange.addEventListener("change", x => {
    rangeToColor(x);
    body.style.backgroundColor = "rgb("+r+","+g+","+b+")";
});
fontColorRange.addEventListener("change", x => {
    rangeToColor(x);
    body.style.color = "rgb("+r+","+g+","+b+")";
});
buttonColorRange.addEventListener("change", x => {
    rangeToColor(x);
    uiButtons.forEach(elem => { 
        elem.style.backgroundColor = "rgb("+r+","+g+","+b+")";
    });
});
buttonFontColorRange.addEventListener("change", x => {
    rangeToColor(x);
    uiButtons.forEach(elem => { 
        elem.style.backgroundColor = "rgb("+r+","+g+","+b+")";
    });
});
keyboardColorRange.addEventListener("change", x => {
    rangeToColor(x);
    inputLetters.forEach(elem => { 
        elem.style.backgroundColor = "rgb("+r+","+g+","+b+")";
    });
});
keyboardFontColorRange.addEventListener("change", x => {
    rangeToColor(x);
    inputLetters.forEach(elem => { 
        elem.style.color = "rgb("+r+","+g+","+b+")";
    });
});
menuBgColorRange.addEventListener("change", x => {
    rangeToColor(x);
    navbar.style.backgroundColor = "rgb("+r+","+g+","+b+")";
    submenuItems.forEach(elem => { 
        elem.style.backgroundColor = "rgb("+r+","+g+","+b+")";
    });
});
menuAccentColorRange.addEventListener("change", x => {
    rangeToColor(x);
    menuItems.forEach(elem => { 
        elem.style.backgroundColor = "rgb("+r+","+g+","+b+")";
    });
});
menuFontColorRange.addEventListener("change", x => {
    rangeToColor(x);
    navbar.style.color = "rgb("+r+","+g+","+b+")";
    submenuItems.forEach(elem => { 
        elem.style.color = "rgb("+r+","+g+","+b+")";
    });
    menuItems.forEach(elem => { 
        elem.style.backgroundColor = "rgb("+r+","+g+","+b+")";
    });
});
shiftGameColorRange.addEventListener("change", x => {
    rangeToColor(x);
    squares.forEach(elem => { 
        elem.style.backgroundColor = "rgb("+r+","+g+","+b+")";
    });
});
shiftGameFontColorRange.addEventListener("change", x => {
    rangeToColor(x);
    squares.forEach(elem => { 
        elem.style.color = "rgb("+r+","+g+","+b+")";
    });
});


function rangeToColor(event){
    if(lastColorChanger != event.target.parentElement)
        resetColor();
    lastColorChanger = event.target.parentElement;

    let rangeValue = event.target.value;

    if(event.target.className === 'red') {
        r = rangeValue;
        event.target.parentElement.querySelector('.redVal').innerHTML = r;
    }
    if(event.target.className === 'green') {
        g = rangeValue;
        event.target.parentElement.querySelector('.greenVal').innerHTML = g;
    }
    if(event.target.className === 'blue') {
        b = rangeValue;
        event.target.parentElement.querySelector('.blueVal').innerHTML = b;
    }
}

function resetColor() {
    r = 0;
    g = 0;
    b = 0;
}

/*hero name */
inputLettersContainer.addEventListener("click", event => {
    if(event.target.innerHTML === 'Del') {
        heroName = heroName.substring(0, heroName.length-1);
        nameOutput.innerHTML = heroName;
    } else if(event.target.innerHTML.length === 1) {
        heroName += event.target.innerHTML;
        nameOutput.innerHTML = heroName;
    }
});

/* key input */
inputNumbersContainer.addEventListener("click", event => {
    if(event.target.innerHTML === 'Del') {
        keyInput = keyInput.substring(0, keyInput.length-1);
        key.innerHTML = keyInput;
    } else if(event.target.innerHTML.length === 1) {
        keyInput += event.target.innerHTML;
        key.innerHTML = keyInput;
    }
});

keySubmitButton.addEventListener("click", x => {
    if(key.innerHTML === '1234') {
        hideGame.classList.remove('none');
        hideEnterKeyContainer.classList.add('none');
        //reorderSquares(); //start shiftGame at random order
        //render();
    } else {
        pointLoss();
    }
});

/* exploding button */
buttonExplosionContainer.addEventListener("click", explodingButton);

let explodeCnt = 1;
let likelihood = 0;
function explodingButton(event) {
    let depth = event.composedPath().length - 7;
    likelihood = Math.random();
    let r = Math.floor(Math.random() * 255);
    let g = Math.floor(Math.random() * 255);
    let b = Math.floor(Math.random() * 255);

    if(explodeCnt === 1){
        likelihood = 1;
    }
    if(likelihood > 0.5){
        event.target.innerHTML = `
        <div class="explodeButton" style="background-color: rgb(${r},${g},${b})">Go to Key!<span class="explodeButtonDesc">${20/(2 ** depth)}%</span></div>
        <div class="explodeButton" style="background-color: rgb(${r},${g},${b})">Go to Key!<span class="explodeButtonDesc">${20/(2 ** depth)}%</span></div>
        <div class="explodeButton" style="background-color: rgb(${r},${g},${b})">Go to Key!<span class="explodeButtonDesc">${20/(2 ** depth)}%</span></div>`

        event.target.classList.add("buttonExplosion");
        event.target.classList.remove("explodeButton");
        event.target.style.backgroundColor = "rgb(255,255,255)";
    }
    if(likelihood * 100 < 20/(2 ** depth)){
        window.location.assign("HTML/thekey.html");
    }
    explodeCnt++;
    pointLoss();
}

/* shift game */
squareContainer.addEventListener("click", shiftGame);

function render() {
    squareContainerContent.innerHTML = '';
    let squareButtonsArrayEntry = '';
    for (let i = 0; i < squareButtonsArrayCopy.length; i++){
        squareButtonsArrayEntry += `<div data-index="${i}" class ="square ${squareButtonsArrayCopy[i].innerHTML === 'A' ? `empty-square` : ''}">${squareButtonsArrayCopy[i].innerHTML}${squareButtonsArrayCopy[i].innerHTML === 'S' && helpCnt > 0 ? '.' : ''}</div>`
    }
    squareContainerContent.innerHTML = squareButtonsArrayEntry;
}

function swapSquares(pressedSquareIndex, emptySquareIndex) {
    squareButtonsArrayChange[pressedSquareIndex] = squareButtonsArrayCopy[emptySquareIndex];
    squareButtonsArrayChange[emptySquareIndex] = squareButtonsArrayCopy[pressedSquareIndex];
    squareButtonsArrayCopy = [...squareButtonsArrayChange];
}

/* Randomly reorder squares using the swap method, so that game is always beatable */
function reorderSquares() {
    for(let j = 0; j <= Math.floor(Math.random() * 10000); j++) {
        if(Math.floor(Math.random() * 11) < 5) {
            for (let i = 0; i < squareButtonsArrayChange.length; i++) {
                let emptySquareIndex;
                squareButtonsArrayCopy.forEach((square, index) => {
                    if (square.innerHTML === 'A')
                        emptySquareIndex = index;
                });
                if ((Math.abs(i - emptySquareIndex) === 1) || (Math.abs(i - emptySquareIndex) === 3)) {
                    swapSquares(i, emptySquareIndex);
                    break;
                }
            }
        } else {
            for (let i = squareButtonsArrayChange.length - 1; i >= 0; i--) {
                let emptySquareIndex;
                squareButtonsArrayCopy.forEach((square, index) => {
                    if (square.innerHTML === 'A')
                        emptySquareIndex = index;
                });
                if ((Math.abs(i - emptySquareIndex) === 1) || (Math.abs(i - emptySquareIndex) === 3)) {
                    swapSquares(i, emptySquareIndex);
                    break;
                }
            }
        }
    }
}
/* Rules: All possible move other 1 or 3 entries away in the array */
function shiftRules(pressedSquareIndex, emptySquareIndex) {
    if((Math.abs(pressedSquareIndex - emptySquareIndex) === 1) || (Math.abs(pressedSquareIndex - emptySquareIndex) === 3) ){
        swapSquares(pressedSquareIndex, emptySquareIndex);
    } else if (Math.abs(pressedSquareIndex - emptySquareIndex) === 0) {
    } else {
        reorderSquares(); //Reorder squares, if forbidden move
        pointLoss();
    }
}
function shiftGame(event) {
    let pressedSquareIndex = event.target.dataset.index; //Get indices for pressed square and empty square to check if swap allowed
    let emptySquareIndex;

    squareButtonsArrayCopy.forEach((square, index) => {
        if(square.innerHTML === 'A')
            emptySquareIndex = index;
    });

    shiftRules(pressedSquareIndex, emptySquareIndex); //Check rules and decide outcome
    render();                                          //Render

    /* Check for and define what happens when you beat the game */
    let gameWin = true;
    let i = squareButtonsArrayCopy.length;
    while (i--) {
        if(squareButtonsArrayCopy[i] !== squareButtonsArray[i])
            gameWin = false;
    }
    if(gameWin){
        squareContainer.removeEventListener("click", shiftGame);
        document.getElementById("game-message").innerHTML = `<strong>Congratulations ${heroName}!</strong><br>You beat the game with ${points} points.`
    }
}

/* point loss */
function pointLoss() {
    points--;
    pointsDiv.innerHTML = `${points} Punkte`;
    pointsDiv.classList.add("wiggle");
    setTimeout(e => {pointsDiv.classList.remove("wiggle");}, 500);
}

/* help for shift game */
helpButton.addEventListener("click", shiftHelp);

function shiftHelp() {
    let cheatSheet = document.getElementById('cheatSheet');
    helpCnt++;
    pointLoss();
    switch(helpCnt) {
        case 1: render();
            break;
        case 2: document.getElementById('game-message').classList.remove('hidden');
            break;
        case 3: cheatSheet.classList.remove('hidden');
            cheatSheet.innerHTML += `<br><br>(Rows,Columns)...<br><br>1. Bring the first square to (1,1)<br>`;
            break;
        case 4: cheatSheet.innerHTML += `2. Bring the third square to (1,2)<br>`;
            break;
        case 5: cheatSheet.innerHTML += `3. Bring the second square to (2,2)<br>`;
            break;
        case 6: cheatSheet.innerHTML += `4. Rotate second and third squares until they're in the correct spots<br>`;
            break;
        case 7: cheatSheet.innerHTML += `5. Bring the seventh square to (2,1)<br>`;
            break;
        case 8: cheatSheet.innerHTML += `6. Bring the fourth square to (2,2)<br>`;
            break;
        case 9: cheatSheet.innerHTML += `7. Rotate seventh and fourth square until they're in the correct spots<br>`;
            break;
        case 10: cheatSheet.innerHTML += `8. Rotate the fifth, sixth and eighth squares into their final positions<br><br><strong>Done!</strong>`;
            break;
    }
}